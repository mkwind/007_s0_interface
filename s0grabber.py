import os
import RPi.GPIO as GPIO
import time
import random
import requests
import json

### fields ####################################################################
### genral ###
elapsed = 0             # stores cumulate delta time
last_state = False      # for comparing state
current_state = False   # for comparing state    

### counters ###
number_10sec = 0        # stores impulses count in 10 seconds
number_01min = 0        # stores impulses count in 1 minute
number_05min = 0        # stores impulses count in 5 minutes
number_15min = 0        # stores impulses count in 15 minutes

# timers
last_time = 0           # use with calculation of delta time
last_time_10sec = 0     # stores last time, the 10 sec counter was reseted
last_time_01min = 0     # stores last time, the 1 min counter was reseted
last_time_05min = 0     # stores last time, the 5 min counter was reseted
last_time_15min = 0     # stores last time, the 15 min counter was reseted

# written values store
write_number_10sec = 0  # stores the last impuses count for the 10 sec
write_number_01min = 0  # stores the last impuses count for the 01 min, so it can be pushed every 10 sec to the webserver
write_number_05min = 0  # stores the last impuses count for the 05 min, so it can be pushed every 10 sec to the webserver
write_number_15min = 0  # stores the last impuses count for the 15 min, so it can be pushed every 10 sec to the webserver

######
### This function calculates a delta time value between a current time
### value and a GLOBAL last time value. Because it is easier to calculate
### with milliseconds than fractions of seconds
###
### return the delta time as milliseconds (and fractions of it)
def calc_delta_time(current_time):
    global last_time                            # global last time value
    delta_time = current_time - last_time       
    last_time = current_time
    return delta_time * 1000                    # multiply to get milliseconds

#######
### This function counts the impulses within 10 seconds and stores them
### to a global field
def calc_10sec_impulses(current_time):
    global last_time_10sec                              # using global value
    global number_10sec                                 # using global value
    global write_number_10sec                           # using global value

    delta_time = (current_time - last_time_10sec)       # delta time stores fraction of seconds
    is10sec = (current_time // 1) % 10 == 0             # get whole seconds

    # only execute, if seconds in range of 0,10,20,30,40,50 
    if delta_time > 10 and is10sec:                     
        last_time_10sec = current_time
        write_number_10sec = number_10sec
        write_values()
        number_10sec = 0
       
#######
### This function counts the impulses within 1 minute and stores them
### to a global field
def calc_01min_impulses(current_time):
    global last_time_01min                                  # using global value
    global number_01min                                     # using global value        
    global write_number_01min                               # using global value

    delta_time = (current_time - last_time_01min) / 60      # delta time stores fraction of minutes
    is01min = (current_time // 1) % 60 == 0                 # get whole minutes

    # executed every minute
    if delta_time > 1 and is01min:
        last_time_01min = current_time
        write_number_01min = number_01min
        number_01min = 0
        

#######
### This function counts the impulses within 5 minutes and stores them
### to a global field
def calc_05min_impulses(current_time):
    global last_time_05min                                  # using global value
    global number_05min                                     # using global value
    global write_number_05min                               # using global value

    delta_time = (current_time - last_time_05min) / 60      # delta time as fraction of minutes
    is05min = (current_time // 1) % 300 == 0                # insure a correct 5 min step
    
    # executed every 5 minuntes (0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 55 )
    if delta_time > 5 and is05min:
        last_time_05min = current_time
        write_number_05min = number_05min
        number_05min = 0
        
#######
### This function counts the impulses within 15 minutes and stores them
### to a global field
def calc_15min_impulses(current_time):
    global last_time_15min                                  # using global value
    global number_15min                                     # using global value
    global write_number_15min                               # using global value

    delta_time = (current_time - last_time_15min) / 60      # delta time as fraction of minutes
    is15min = (current_time // 1) % 900 == 0                # insure a correct 15 min step

    # executed every 15 min (0, 15, 30, 45)
    if delta_time > 15 and is15min:
        last_time_15min = current_time
        write_number_15min = number_15min
        number_15min = 0
        print("15 min count complete " + str(current_time) + "\n")
        

#######
### This function post the values to a webserver as json payload
def write_values():
    global write_number_10sec
    global write_number_01min
    global write_number_05min
    global write_number_15min
    global last_time_10sec
    global last_time_01min
    global last_time_05min
    global last_time_15min
    global current_time
        

    # build the payload from the global fields    
    payload = {
        'value_10_sec': str(write_number_10sec),
        'time_10_sec': str(int(last_time_10sec//1)),
        'value_01_min': str(write_number_01min),
        'time_01_min': str(int(last_time_01min//1)),
        'value_05_min': str(write_number_05min),
        'time_05_min': str(int(last_time_05min//1)),
        'value_15_min': str(write_number_15min),
        'time_15_min': str(int(last_time_15min//1))
    }
    try:
        url = 'http://localhost:5000/api/values/set'                        # the destination
        headers = {'content-type': 'application/json'}                      # +must+ content type
        r = requests.post (url, data=json.dumps(payload), headers=headers)  # post
    except:
        print("could not send data.")


### setup #####################################################################
GPIO.setmode(GPIO.BCM)                                  # set pin layout
GPIO.setup(25, GPIO.IN, pull_up_down=GPIO.PUD_UP)       # configure input pin
last_time = time.time()
last_time_10sec = 0
last_time_01min = 0
last_time_05min = 0
last_time_15min = 0

### main loop ###
while True:
    current_time = time.time()                          # get current time for calculations

    calc_10sec_impulses(current_time)                   # count impulses
    calc_01min_impulses(current_time)
    calc_05min_impulses(current_time)
    calc_15min_impulses(current_time)

    elapsed += calc_delta_time(current_time)            # count elesed time since last count
    current_state = GPIO.input(25)                      # read the gpio state
    if current_state != last_state and elapsed > 45:    # if gpio state has change outside of a
        elapsed = 0                                     # arbitrary time span (here set to 45 ms)

        number_10sec += 1                               # count impulses
        number_01min += 1
        number_05min += 1
        number_15min += 1

        last_state = current_state                      # store current state for future comparisons
        
        
        

        

