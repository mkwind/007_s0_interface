# -*- coding: utf-8 -*-
import os
import re

# Retrieve Flask, our framework
from flask import Flask
from flask import jsonify
from flask import request

#######################################################################################################################
### global values 

value_10_sec = 0
time_10_sec = 0
value_01_min = 0
time_01_min = 0
value_05_min = 0
time_05_min = 0
value_15_min = 0
time_15_min = 0
timestamp = 0
list_15_min_values = []
main_directory = '/home/pi/Development/python/prod/s0_grabber/'
currentfilename = ''




#######################################################################################################################
### custom functions

def get_value_for(property, content):
    pattern = property + ": "
    for line in content:
        line_match = re.search(pattern + "\d+", line)
        if line_match:
            temp = re.search(": \d+", line_match.group())
            number = re.search("\d+", temp.group())
            return number.group()
    
def store_15_min_value(value, time, time_compare):
    if time_compare == time:
        return

    global currentfilename
    if time % (24*60*60) == 0 or currentfilename == '':
        currentfilename = str(time) + '.txt'
    list_15_min_values.append([time, value])

    f = None
    try:
        f = open(main_directory + 'data/' + currentfilename, 'a+')
    except:
        try:
            f = open(os.path.join('data',currentfilename),'a+')
        except:
            print('error append file')

    f.write('{}:{}\n'.format(time, value))
    f.close()
    return






#######################################################################################################################
### create the Flask app
app = Flask(__name__)




#######################################################################################################################
### routed function

# Main Page Route
@app.route("/")
def index():
    return "Hello World!<br/><br/><a href='/main'>Visit main</a>"

# route to query the values
@app.route("/api/values")
def api_values():
    return jsonify(
        {
            'value_10_sec': value_10_sec,
            'value_01_min': value_01_min,
            'value_05_min': value_05_min,
            'value_15_min': value_15_min,
            'time_10_sec': time_10_sec,
            'time_01_min': time_01_min,
            'time_05_min': time_05_min,
            'time_15_min': time_15_min
        })

# route for posting values
@app.route('/api/values/set', methods = ['POST'])
def post():
    global value_10_sec, value_01_min, value_05_min, value_15_min
    global time_10_sec, time_01_min, time_05_min, time_15_min

    # Get the parsed contents of the form data
    data = request.json

    # preserve 15min values
    store_15_min_value(value_10_sec, int(data['time_15_min']), time_15_min)

    value_10_sec = int(data['value_10_sec'])
    time_10_sec =  int(data['time_10_sec'])
    value_01_min =  int(data['value_01_min'])
    time_01_min =  int(data['time_01_min'])
    value_05_min =  int(data['value_05_min'])
    time_05_min =  int(data['time_05_min'])
    value_15_min =  int(data['value_15_min'])
    time_15_min =  int(data['time_15_min'])

    # return transmitted data for confirmation
    return jsonify(data)

# Second Page Route
@app.route("/main")
def main():
    html = None
    try:
        html = open(main_directory + 'web/main.html')
    except:
        try:
            html = open(os.path.join('web','main.html'))
        except:
            return 'file not found'
    return html.read()




#######################################################################################################################
### main 

# start the webserver
if __name__ == "__main__":
    app.debug = True
    app.run()
    #app.run(host='0.0.0.0', port=80, debug=False)
